FROM openjdk:16
EXPOSE 8080
ADD target/getRoof-0.0.1-SNAPSHOT.jar getRoof-0.0.1-SNAPSHOT.jar
#COPY target/smart_twitter-0.0.1.jar smart_twitter-0.0.1.jar
CMD ["java","-jar","getRoof-0.0.1-SNAPSHOT.jar"]
#ENTRYPOINT ["java","-jar","smart_twitter-0.0.1.jar"]