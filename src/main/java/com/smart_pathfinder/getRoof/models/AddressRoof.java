package com.smart_pathfinder.getRoof.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "address_roofs")
public class AddressRoof extends Address {
    public AddressRoof(String streetAddress, String houseNumber, String city, String state, String zipCode) {
        super(streetAddress, houseNumber, city, state, zipCode);
    }
}
