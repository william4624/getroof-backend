package com.smart_pathfinder.getRoof.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "address_users")
public class AddressUser extends Address {
    public AddressUser(String streetAddress, String houseNumber, String city, String state, String zipCode) {
        super(streetAddress, houseNumber, city, state, zipCode);
    }
}
