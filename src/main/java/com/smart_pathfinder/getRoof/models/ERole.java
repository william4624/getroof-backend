package com.smart_pathfinder.getRoof.models;

public enum ERole {
    ROLE_USER,
    ROLE_EDIT,
    ROLE_CREATE,
    ROLE_DELETE,
    ROLE_ADMIN
}
