package com.smart_pathfinder.getRoof.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * @author William Kana
 * @on 12/7/2021
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
@MappedSuperclass
public abstract class FileBaseBean implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "size")
    private long size;

    @Lob
    @Column(name="data")
    private byte[] data;

    @Column(name="created_at")
    private LocalDateTime createdAt;

    @Column(name="updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "compressed_Size")
    private Integer compressedSize;

    @Column(name = "by")
    private UUID by;

    public void compressData() {
        this.size = this.data.length;

        Deflater deflater = new Deflater();
        deflater.setInput(this.data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        this.compressedSize = outputStream.size();
        this.data = outputStream.toByteArray();
    }

    public void deCompressData() {
        Inflater inflater = new Inflater();
        inflater.setInput(this.data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException e) {
        } catch (DataFormatException e2) {
        }
        this.data = outputStream.toByteArray();
    }

    public FileBaseBean(String name, String type, long size, byte[] data, LocalDateTime createdAt, UUID by) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.data = data;
        this.createdAt = createdAt;
        this.by = by;
    }
}
