package com.smart_pathfinder.getRoof.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author William Kana
 * @on 12/7/2021
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "users_files")
public class UserFile extends FileBaseBean {
    public UserFile(String name, String type, long size, byte[] data, LocalDateTime createdAt, UUID by) {
        super(name, type, size, data, createdAt, by);
    }
}
