package com.smart_pathfinder.getRoof.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "roofs")
public class Roof implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Size(min = 3)
    @Column(name = "title", nullable = false, length = 1000)
    private String title;

    @Column(name = "number_bedroom", nullable = false)
    private int numberBedroom;

    @Range(min=0, max=100)
    @Column(name = "living_room", nullable = false)
    private int numberLivingRoom;

    // price for one day
    @Column(name = "price", nullable = false)
    private int price;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private ECurrency currency;

    @Column(name = "description", nullable = false, length = 5000)
    private String description;

    @Range(min=0, max=100)
    @Column(name = "promo_percentage")
    private Integer promoPercentage;

    // price for one day
    // after promo
    @Column(name = "promo_price")
    private int promoPrice;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    // status 0 = deleted,  1 = available, 2 = booked and 3 = in_cleaning
    @Column(name = "status")
    private byte status = 1;

    /*@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "roofs_rtypess",
            joinColumns = @JoinColumn(
                    name = "roof_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "rtype_id", referencedColumnName = "id"))
    private Set<RType> rTypes = new HashSet<>();*/

    @OneToMany(targetEntity = RoofType.class, cascade=CascadeType.ALL)
    @JoinColumn(name = "roof_type", nullable = true)
    private Set<RoofType> types = new HashSet<>();

    @OneToOne(targetEntity = AddressRoof.class,cascade=CascadeType.ALL)
    @JoinColumn(name = "roof_address_id", nullable = false)
    private AddressRoof address;

    // owner
    @ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(targetEntity = LanguageRoof.class, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinColumn(name = "roof_languages_id", nullable = false)
    private Set<LanguageRoof> availableLanguages;

    @OneToMany(targetEntity = RoofFile.class,  mappedBy = "by", cascade=CascadeType.ALL)
    private List<RoofFile> roofFiles;

    public void applyPromoCode() {
        if (this.promoPercentage != null)
            this.promoPrice =  (this.price * this.promoPercentage) / 100;
    }

   /* public int getPromoPrice() {
        if (this.promoPercentage != null)
                return (this.price * this.promoPercentage) / 100;
        return promoPrice;
    }*/

    public User getUser() {
        return new User(user.getId(), user.getUsername(), user.getEmail(), user.getPhoneNumbers(), user.getFirstName(),
                user.getLastName(), user.getDescription(), user.getLanguages(), user.getUserFiles());
    }
}
