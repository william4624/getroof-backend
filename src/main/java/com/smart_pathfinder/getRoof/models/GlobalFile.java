package com.smart_pathfinder.getRoof.models;

/**
 * @author William Kana
 * @on 12/7/2021
 */

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "global_files")
public class GlobalFile extends FileBaseBean {
    public GlobalFile(String name, String type, long size, byte[] data, LocalDateTime createdAt, UUID by) {
        super(name, type, size, data, createdAt, by);
    }
}
