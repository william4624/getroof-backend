package com.smart_pathfinder.getRoof;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetRoofApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetRoofApplication.class, args);
	}

}
