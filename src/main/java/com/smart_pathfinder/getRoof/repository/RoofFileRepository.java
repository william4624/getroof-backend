package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.RoofFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoofFileRepository extends JpaRepository<RoofFile, UUID> {
}