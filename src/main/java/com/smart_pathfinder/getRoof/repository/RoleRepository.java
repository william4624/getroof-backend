package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.ERole;
import com.smart_pathfinder.getRoof.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Optional<Role> findByName(ERole name);
}
