package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.LanguageRoof;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LanguageRoofRepository extends JpaRepository<LanguageRoof, UUID> {
}