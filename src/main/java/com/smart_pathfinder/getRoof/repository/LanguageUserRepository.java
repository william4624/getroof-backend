package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.LanguageUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LanguageUserRepository extends JpaRepository<LanguageUser, UUID> {
}