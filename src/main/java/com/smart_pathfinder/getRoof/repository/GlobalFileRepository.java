package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.GlobalFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GlobalFileRepository extends JpaRepository<GlobalFile, UUID> {
}