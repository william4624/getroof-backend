package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.UserFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserFileRepository extends JpaRepository<UserFile, UUID> {
}