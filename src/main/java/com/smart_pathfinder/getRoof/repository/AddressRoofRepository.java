package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.AddressRoof;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AddressRoofRepository extends JpaRepository<AddressRoof, UUID> {
}