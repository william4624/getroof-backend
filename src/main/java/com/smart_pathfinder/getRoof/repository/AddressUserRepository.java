package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.AddressUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AddressUserRepository extends JpaRepository<AddressUser, UUID> {
}