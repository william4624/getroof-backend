package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.Roof;
import com.smart_pathfinder.getRoof.payload.request.FilterQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RoofRepository extends JpaRepository<Roof, UUID> {
    Page<Roof> findAllByStatus(byte status, Pageable pageable);

    @Query("SELECT r.id, r.numberBedroom, r.numberLivingRoom, r.price, r.currency, " +
            "r.description, r.promoPercentage, r.promoPrice, r.types, " +
            "r.address, r.user, r.availableLanguages " +
            "FROM Roof r WHERE r.id= :roofId")
    Optional<Roof> findByIdWithRole(@Param("roofId") UUID roofId);

    Optional<Roof> findRoofById(UUID id);

    Optional<Roof> findRoofByIdAndUserId(UUID id, UUID userId);

    Page<Roof> findAllByUserId(UUID userId, Pageable pageable);

    Page<Roof> findRoofsByTitleContaining(String title, Pageable pageable);
}
