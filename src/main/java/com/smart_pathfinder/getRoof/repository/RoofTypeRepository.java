package com.smart_pathfinder.getRoof.repository;

import com.smart_pathfinder.getRoof.models.RoofType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoofTypeRepository extends JpaRepository<RoofType, UUID> {
}