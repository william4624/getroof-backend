package com.smart_pathfinder.getRoof.payload.response;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FileResponse implements Serializable {
    private UUID id;
    private String name;
    private String type;
    private long size;
    private byte[] data;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
