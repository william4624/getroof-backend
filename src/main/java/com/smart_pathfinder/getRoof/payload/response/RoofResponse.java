package com.smart_pathfinder.getRoof.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smart_pathfinder.getRoof.models.Roof;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RoofResponse implements Serializable {

    @JsonProperty("currentPage")
    private int currentPage = 0;

    @JsonProperty("totalItems")
    private long totalItems = 0;

    @JsonProperty("totalPages")
    private int totalPages = 0;

    @JsonProperty("body")
    private List<Roof> body = new ArrayList<>();
}
