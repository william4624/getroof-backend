package com.smart_pathfinder.getRoof.payload.request;

import com.smart_pathfinder.getRoof.models.AddressRoof;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RoofRequest {
    private String title;

    private int numberBedroom;

    private int numberLivingRoom;

    private int price;

    private String currency;

    private String description;

    private Integer promoPercentage;

    private Set<String> types;

    private AddressRoof address;

    private Set<String> availableLanguages;
}
