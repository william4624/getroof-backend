package com.smart_pathfinder.getRoof.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FilterQuery {
    @JsonProperty("roofId")
    private UUID roofId;

    @JsonProperty("userId")
    private UUID userId;

    @JsonProperty("page")
    private int page = 0;

    @JsonProperty("size")
    private int size = 3;

    @JsonProperty("streetAddress")
    private String streetAddress;

    @JsonProperty("houseNumber")
    private Integer houseNumber;

    @JsonProperty("city")
    private String city;

    @JsonProperty("state")
    private String state;

    @JsonProperty("zipCode")
    private String zipCode;

}
