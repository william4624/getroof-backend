package com.smart_pathfinder.getRoof.payload.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.smart_pathfinder.getRoof.models.AddressUser;
import lombok.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {
    private String username;

    private String email;

    private String password;

    private String firstName;

    private String lastName;

    private int age;

    @JsonAlias({"dateOfBirth", "bornOn"})
    @JsonFormat(pattern = "dd-MM-yyyy") //  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDate dateOfBirth;

    private String description;

    private Set<String> roles;

    private Set<String> phoneNumbers;

    private AddressUser address;

    private Set<String> languages;
}
