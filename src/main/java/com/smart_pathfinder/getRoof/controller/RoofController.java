package com.smart_pathfinder.getRoof.controller;

import com.smart_pathfinder.getRoof.models.Roof;
import com.smart_pathfinder.getRoof.payload.request.RoofRequest;
import com.smart_pathfinder.getRoof.payload.response.RoofResponse;
import com.smart_pathfinder.getRoof.services.RoofService;
import com.smart_pathfinder.getRoof.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/roofs")
public class RoofController {
    private RoofService roofService;
    private UserService userService;

    public RoofController(RoofService roofService, UserService userService) {
        this.roofService = roofService;
        this.userService = userService;
    }

    @GetMapping("/standard_list")
    public ResponseEntity<RoofResponse> getRoofStandard(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ) {
        return new ResponseEntity<>(roofService.getRoofStandard(page, size), HttpStatus.OK);
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> createRoof(@Valid @RequestBody RoofRequest roofRequest) {
        return ResponseEntity.ok(roofService.createRoof(roofRequest));
    }

    @RequestMapping(value = "/update", method = { RequestMethod.PATCH, RequestMethod.POST})
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> updateRoof(@Valid @RequestBody Roof roof) {
        return ResponseEntity.ok(roofService.updateRoof(roof));
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> deleteRoof(
            @PathVariable("id") String id
    ) {
        try {
            roofService.deleteRoof(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/roof_view_public/{id}")
    public ResponseEntity<Roof> getRoofByIdPublic(
            @PathVariable("id") String id
    ) {
        return new ResponseEntity<>(roofService.getRoofById(false, id), HttpStatus.OK);
    }

    @GetMapping("/roof_view_private/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<Roof> getRoofByIdPrivate(
            @PathVariable("id") String id
    ) {
        return new ResponseEntity<>(roofService.getRoofById(true, id), HttpStatus.OK);
    }

    @GetMapping("/roofs_by_user_id_view_public")
    public ResponseEntity<RoofResponse> getRoofsByUserIdPublic(
            @RequestParam(name = "userId") String userId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ) {
        return new ResponseEntity<>(roofService.getRoofsByUserId(false, userId, page, size), HttpStatus.OK);
    }

    @GetMapping("/roofs_by_user_id_view_private")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<RoofResponse> getRoofsByUserIdPrivate(
            @RequestParam(name = "userId") String userId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ) {
        return new ResponseEntity<>(roofService.getRoofsByUserId(false, userId, page, size), HttpStatus.OK);
    }

    @RequestMapping(value = "/book_or_cancel", method = { RequestMethod.PATCH, RequestMethod.POST})
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<Roof> bookRoof(
            @RequestParam(name = "id") String id,
            @RequestParam(name = "operation") int operation
    ) {
        try {
            return new ResponseEntity<>(roofService.bookOrCancelRoof(operation, id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
