package com.smart_pathfinder.getRoof.controller;


import com.smart_pathfinder.getRoof.exception.TokenRefreshException;
import com.smart_pathfinder.getRoof.models.RefreshToken;
import com.smart_pathfinder.getRoof.payload.request.LoginRequest;
import com.smart_pathfinder.getRoof.payload.request.SignupRequest;
import com.smart_pathfinder.getRoof.payload.request.TokenRefreshRequest;
import com.smart_pathfinder.getRoof.payload.response.MessageResponse;
import com.smart_pathfinder.getRoof.payload.response.TokenRefreshResponse;
import com.smart_pathfinder.getRoof.services.RefreshTokenService;
import com.smart_pathfinder.getRoof.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    UserService userService;
    RefreshTokenService refreshTokenService;

    public AuthController(UserService userService, RefreshTokenService refreshTokenService)
    {
        this.userService = userService;
        this.refreshTokenService = refreshTokenService;
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(userService.login(loginRequest));
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<?> registerUser(@RequestBody SignupRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        return ResponseEntity.ok(userService.register(signUpRequest));
    }

    @PostMapping("/refreshToken")
    public ResponseEntity<?> refreshToken(@Valid @RequestBody TokenRefreshRequest request) {
        String requestRefreshToken = request.getRefreshToken();
        if (requestRefreshToken == null) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: refreshToken is not present!"));
        }

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String token = userService.generateTokenFromUsername(user);
                    return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
                })
                .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
                        "Refresh token is not in database!"));
    }
}
