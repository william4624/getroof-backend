package com.smart_pathfinder.getRoof.controller;

import com.smart_pathfinder.getRoof.models.User;
import com.smart_pathfinder.getRoof.payload.response.FileResponse;
import com.smart_pathfinder.getRoof.services.GlobalService;
import com.smart_pathfinder.getRoof.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/global")
public class GlobalController {
    GlobalService globalService;
    UserService userService;


    public GlobalController(
            GlobalService globalService, UserService userService
    ) {
        this.globalService = globalService;
        this.userService = userService;
    }

    @PostMapping("/upload")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<FileResponse> uploadFile(
            @RequestParam(name = "entity") String entity,
            @RequestParam("file") MultipartFile file
    ) throws IOException {

        User user = userService.getCurrentUser();
        FileResponse fileResponse = globalService.storeFile(file, user.getId(), entity);
        return ResponseEntity.ok(fileResponse);
    }
}
