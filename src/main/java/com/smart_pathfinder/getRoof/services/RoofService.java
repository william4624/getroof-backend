package com.smart_pathfinder.getRoof.services;

import com.smart_pathfinder.getRoof.exception.ResourceNotFoundException;
import com.smart_pathfinder.getRoof.models.*;
import com.smart_pathfinder.getRoof.payload.request.RoofRequest;
import com.smart_pathfinder.getRoof.payload.response.RoofResponse;
import com.smart_pathfinder.getRoof.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RoofService implements com.smart_pathfinder.getRoof.interfaces.RoofService {
    // https://www.bezkoder.com/spring-boot-upload-file-database/
    // private static final Logger logger = LogManager.getLogger();
    private static final Logger log = LoggerFactory.getLogger(RoofService.class);

    private RoofRepository roofRepository;
    private RoofTypeRepository roofTypeRepository;
    private AddressRoofRepository addressRoofRepository;
    private LanguageRoofRepository languageRoofRepository;
    private UserRepository userRepository;
    private UserService userService;

    public RoofService(
            RoofRepository roofRepository, RoofTypeRepository roofTypeRepository,
            AddressRoofRepository addressRoofRepository, LanguageRoofRepository languageRoofRepository,
            UserRepository userRepository, UserService userService) {
        this.roofRepository = roofRepository;
        this.roofTypeRepository = roofTypeRepository;
        this.addressRoofRepository = addressRoofRepository;
        this.languageRoofRepository = languageRoofRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    // Add Paging
    public RoofResponse getRoofStandard(int page, int size) {
        log.info("Getting Standard Roofs");
        Pageable paging = PageRequest.of(page, size);
        Page<Roof> roofPage;
        roofPage = roofRepository.findAllByStatus((byte) 1, paging);
        return new RoofResponse(roofPage.getNumber(), roofPage.getTotalElements(), roofPage.getTotalPages(), roofPage.getContent());
    }

    @Transactional
    public Roof createRoof(RoofRequest roofRequest) {
        Roof roof = new Roof();
        roof.setTitle(roofRequest.getTitle());
        roof.setNumberBedroom(roofRequest.getNumberBedroom());
        roof.setNumberLivingRoom(roofRequest.getNumberLivingRoom());
        roof.setPrice(roofRequest.getPrice());
        roof.setDescription(roofRequest.getDescription());
        roof.setPromoPercentage(roofRequest.getPromoPercentage() != null? roofRequest.getPromoPercentage(): null);
        roof.applyPromoCode();
        roof.setCreatedAt(LocalDateTime.now());
        roof.setAddress(roofRequest.getAddress());

        User user = userRepository.findById(userService.getCurrentUser().getId()).get();
        roof.setUser(user);

        Set<String> strTypes = roofRequest.getTypes();
        Set<RoofType> roofTypes = new HashSet<>();
        if (strTypes == null) {
            roofTypes.add(new RoofType(ERoofType.PRIVATE));
        } else {
            strTypes.forEach(type -> {
                switch (type.toLowerCase()) {
                    case "hostel":
                        roofTypes.add(new RoofType(ERoofType.HOSTEL));

                    default:
                        roofTypes.add(new RoofType(ERoofType.PRIVATE));
                }
            });
        }

        Set<String> languages = roofRequest.getAvailableLanguages();
        Set<LanguageRoof> languageRoofs = new HashSet<>();
        if (languages == null) {
            languageRoofs.add(new LanguageRoof(ELanguage.FR));
        } else {
            languages.forEach(language -> {
                switch (language) {
                    case "en":
                        languageRoofs.add(new LanguageRoof(ELanguage.EN));
                        break;
                    default:
                        languageRoofs.add(new LanguageRoof(ELanguage.FR));
                }
            });
        }

        ECurrency eCurrency = null;
        for (ECurrency c: ECurrency.values()) {
            if (c.name().equals(roofRequest.getCurrency().toUpperCase())) {
                eCurrency = c;
                roof.setCurrency(c);
                break;
            }
        }
        roof.setCurrency(eCurrency != null? eCurrency: ECurrency.FCFA);

        Roof savedRoof = roofRepository.save(roof);
        return savedRoof;
    }

    @Transactional
    public Roof updateRoof(Roof roof) {
        Roof roofFound = roofRepository.findRoofByIdAndUserId(roof.getId(), userService.getCurrentUser().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Not Authorized to update this roof = " + roof.getId()));

        if (roof.getTitle() != null && (roof.getTitle() != roofFound.getTitle())) {
            roofFound.setTitle(roof.getTitle());
        }

        if (roof.getDescription() != null && (roof.getDescription() != roofFound.getDescription())) {
            roofFound.setDescription(roof.getDescription());
        }

        if (roof.getNumberBedroom() != roofFound.getNumberBedroom()) {
            roofFound.setNumberBedroom(roof.getNumberBedroom());
        }

        if (roof.getNumberLivingRoom() != roofFound.getNumberLivingRoom()) {
            roofFound.setNumberLivingRoom(roof.getNumberLivingRoom());
        }

        // to be completed later with other fields
        return roofRepository.save(roofFound);
    }

    public void deleteRoof(String id) {
        Roof roofFound = roofRepository.findRoofByIdAndUserId(UUID.fromString(id), userService.getCurrentUser().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Not Authorized to delete this roof = " + id));
        roofFound.setStatus((byte) 0);
        roofRepository.save(roofFound);
    }

    public Roof getRoofById(boolean status, String id) {
        Roof roof;
        if (!status) {
            roof = roofRepository.findRoofById(UUID.fromString(id))
                    .orElseThrow(() -> new ResourceNotFoundException("Not found Roof with id = " + id));

            User user = roof.getUser();
            roof.setUser(new User (user.getId(), user.getUsername(), user.getFirstName(), user.getLastName()));;
        } else {
            roof = roofRepository.findRoofById(UUID.fromString(id))
                    .orElseThrow(() -> new ResourceNotFoundException("Not found Roof with id = " + id));
        }

        return roof;
    }

    public RoofResponse getRoofsByUserId(boolean status, String userId, int page, int size) throws ResourceNotFoundException {
        Pageable paging = PageRequest.of(page, size);
        Page<Roof> roofPage;
        roofPage = roofRepository.findAllByUserId(UUID.fromString(userId), paging);
        List<Roof> roofs = roofPage.getContent();
        if (!status) {
            roofs = roofs.stream()
                    .map(r -> {
                        Roof roof = r;
                        User user = r.getUser();
                        r.setUser(new User (user.getId(), user.getUsername(), user.getFirstName(), user.getLastName()));
                        return roof;
                    })
                    .collect(Collectors.toList());
        }

        return new RoofResponse(roofPage.getNumber(), roofPage.getTotalElements(), roofPage.getTotalPages(), roofs);
    }

    public Roof bookOrCancelRoof(int operation, String id) {
        Roof roof = roofRepository.findRoofById(UUID.fromString(id))
                .orElseThrow(() -> new ResourceNotFoundException("Not found Roof with id = " + id));

        switch(operation) {
            case 2:
                roof.setStatus((byte) 2);
                break;
            case 3:
                roof.setStatus((byte) 3);
                break;
            default:
                roof.setStatus((byte) 1);
        }
        roofRepository.save(roof);

        return roof;
    }

}
