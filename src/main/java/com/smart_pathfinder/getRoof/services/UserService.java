package com.smart_pathfinder.getRoof.services;

import com.smart_pathfinder.getRoof.models.*;
import com.smart_pathfinder.getRoof.payload.request.LoginRequest;
import com.smart_pathfinder.getRoof.payload.request.SignupRequest;
import com.smart_pathfinder.getRoof.payload.response.JwtResponse;
import com.smart_pathfinder.getRoof.repository.*;
import com.smart_pathfinder.getRoof.security.jwt.JwtUtils;
import com.smart_pathfinder.getRoof.security.services.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);
    private AuthenticationManager authenticationManager;
    private PasswordEncoder encoder;
    private JwtUtils jwtUtils;
    private RefreshTokenService refreshTokenService;
    private UserRepository userRepository;
    private RoleService roleService;

    public Boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public UserService(
            AuthenticationManager authenticationManager, UserRepository userRepository,
            PasswordEncoder encoder, JwtUtils jwtUtils,
            RefreshTokenService refreshTokenService, RoleService roleService,
            RoleRepository roleRepository
            ) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
        this.refreshTokenService = refreshTokenService;
        this.roleService = roleService;
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public String generateTokenFromUsername(User user) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtUtils.generateJwtToken(authentication);
    }

    @Transactional
    public User register(SignupRequest signupRequest) {
        log.info("User Registration starts...");

        User user = new User(signupRequest.getUsername(),
                signupRequest.getEmail(), signupRequest.getFirstName(), signupRequest.getLastName(),
                encoder.encode(signupRequest.getPassword()));

        Set<String> strRoles = signupRequest.getRoles();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleService.findByName(ERole.ROLE_USER);
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleService.findByName(ERole.ROLE_ADMIN);
                        roles.add(adminRole);

                        break;
                    case "create":
                        Role modRole = roleService.findByName(ERole.ROLE_CREATE);
                        roles.add(modRole);


                    case "delete":
                        Role deleteRole = roleService.findByName(ERole.ROLE_DELETE);
                        roles.add(deleteRole);

                    case "edit":
                        Role editRole = roleService.findByName(ERole.ROLE_EDIT);
                        roles.add(editRole);

                        break;
                    default:
                        Role userRole = roleService.findByName(ERole.ROLE_USER);
                        roles.add(userRole);
                }
            });
        }

        List<PhoneNumber> phoneNumbers = new ArrayList<>();
        for (String phoneNumberString : signupRequest.getPhoneNumbers()) {
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setNumber(phoneNumberString);
            phoneNumber.detectNetworkName();
            phoneNumbers.add(phoneNumber);
        }
        user.setPhoneNumbers(phoneNumbers);
        user.setFirstName(signupRequest.getFirstName() != null? signupRequest.getFirstName(): null);
        user.setLastName(signupRequest.getLastName() != null? signupRequest.getLastName(): null);
        user.setAge(signupRequest.getAge());
        user.setDateOfBirth(signupRequest.getDateOfBirth() != null? signupRequest.getDateOfBirth(): null);
        user.setDescription(signupRequest.getDescription() != null? signupRequest.getDescription(): null);
        user.setCreatedAt(LocalDateTime.now());

        List<AddressUser> addressUser = new ArrayList<>();
        addressUser.add(signupRequest.getAddress());
        user.setAddress(addressUser);

        Set<String> languages = signupRequest.getLanguages();
        Set<LanguageUser> languageUsers = new HashSet<>();
        if (languages == null) {
            languageUsers.add(new LanguageUser(ELanguage.FR));
        } else {
            languages.forEach(language -> {
                switch (language) {
                    case "en":
                        languageUsers.add(new LanguageUser(ELanguage.EN));
                        break;
                    default:
                        languageUsers.add(new LanguageUser(ELanguage.FR));
                }
            });
        }
        user.setLanguages(languageUsers);
        user.setRoles(roles);

        log.info("User registered successfully!");
        return userRepository.save(user);
    }

    public JwtResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());

        return new JwtResponse(jwt,
                refreshToken.getToken(),
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles);
    }

    public void logout(String id) {
        refreshTokenService.deleteByUserId(UUID.fromString(id));
    }

    public User getCurrentUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findByUsername(userDetails.getUsername()).get();
        return user;
    }

    public User findUserByUsername(String username) {
        Optional<User> userOptional =  userRepository.findByUsername(username);
        if (userOptional.isPresent())
                return userOptional.get();
        return null;
    }
}
