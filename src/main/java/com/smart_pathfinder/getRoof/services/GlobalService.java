package com.smart_pathfinder.getRoof.services;

import com.smart_pathfinder.getRoof.models.FileBaseBean;
import com.smart_pathfinder.getRoof.models.GlobalFile;
import com.smart_pathfinder.getRoof.models.RoofFile;
import com.smart_pathfinder.getRoof.models.UserFile;
import com.smart_pathfinder.getRoof.payload.response.FileResponse;
import com.smart_pathfinder.getRoof.repository.GlobalFileRepository;
import com.smart_pathfinder.getRoof.repository.RoofFileRepository;
import com.smart_pathfinder.getRoof.repository.UserFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class GlobalService implements com.smart_pathfinder.getRoof.interfaces.GlobalService {
    private static final Logger log = LoggerFactory.getLogger(GlobalService.class);

    private GlobalFileRepository globalFileRepository;
    private UserFileRepository userFileRepository;
    private RoofFileRepository roofFileRepository;

    public GlobalService(
            UserFileRepository userFileRepository, RoofFileRepository roofFileRepository,
            GlobalFileRepository fileGlobalRepository
            ) {
        this.globalFileRepository = fileGlobalRepository;
        this.userFileRepository = userFileRepository;
        this.roofFileRepository = roofFileRepository;
    }

    @Override
    @Transactional
    public FileResponse storeFile(MultipartFile file, UUID by, String entity) throws IOException {
        String fileName;
        if (by != null) {
            fileName = String.valueOf(by);
        } else {
            fileName = file.getName();
        }
        GlobalFile globalFile;
        UserFile userFile;
        RoofFile roofFile;
        FileBaseBean fileBaseBean;
        FileResponse fileResponse = null;


        switch(entity) {
            case "user":
                userFile = new UserFile(fileName, file.getContentType(), file.getSize(), file.getBytes(), LocalDateTime.now(), by);
                userFile.compressData();
                fileBaseBean = userFileRepository.save(userFile);
                break;
            case "roof":
                roofFile = new RoofFile(fileName, file.getContentType(), file.getSize(), file.getBytes(), LocalDateTime.now(), by);
                roofFile.compressData();
                fileBaseBean = roofFileRepository.save(roofFile);
                break;
            default:
                globalFile = new GlobalFile(fileName, file.getContentType(), file.getSize(), file.getBytes(), LocalDateTime.now(), by);
                globalFile.compressData();
                fileBaseBean = globalFileRepository.save(globalFile);
        }

        if (fileBaseBean != null) {
            fileResponse = new FileResponse(fileBaseBean.getId(), fileBaseBean.getName(), fileBaseBean.getType(),
                    fileBaseBean.getSize(), fileBaseBean.getData(), fileBaseBean.getCreatedAt(), fileBaseBean.getUpdatedAt());
        }

        log.info("File Stored");
        return fileResponse;
    }

    @Override
    public void deleteFile(UUID by, String entity) {
        switch(entity) {
            case "user":
                userFileRepository.deleteById(by);
                break;
            case "roof":
                roofFileRepository.deleteById(by);
                break;
            default:
                globalFileRepository.deleteById(by);
        }
    }

    @Override
    public void deleteFiles(List<UUID> bys, String entity) {
        switch(entity) {
            case "user":
                userFileRepository.deleteAllById(bys);
                break;
            case "roof":
                roofFileRepository.deleteAllById(bys);
                break;
            default:
                globalFileRepository.deleteAllById(bys);
        }
    }
}
