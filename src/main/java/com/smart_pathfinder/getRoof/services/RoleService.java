package com.smart_pathfinder.getRoof.services;

import com.smart_pathfinder.getRoof.exception.ResourceNotFoundException;
import com.smart_pathfinder.getRoof.models.ERole;
import com.smart_pathfinder.getRoof.models.Role;
import com.smart_pathfinder.getRoof.repository.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author William K
 * @on 20/12/2021
 */

@Service
public class RoleService {
    private RoleRepository roleRepository;

    public RoleService(
            RoleRepository roleRepository
    ) {
        this.roleRepository = roleRepository;
    }

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Role save(Role role) {
        return roleRepository.save(role);
    }

    public Role findByName(ERole eRole) {
        Role role = roleRepository.findByName(eRole)
                .orElseThrow(() -> new ResourceNotFoundException("Error: Role is not found. Role = " + eRole.name()));

        return role;
    }

    public List<Role> saveAll(List<Role> roles) {
        return roleRepository.saveAll(roles);
    }
}
