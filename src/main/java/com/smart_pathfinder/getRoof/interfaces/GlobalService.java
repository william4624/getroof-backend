package com.smart_pathfinder.getRoof.interfaces;

import com.smart_pathfinder.getRoof.payload.response.FileResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/*
 * @author William Kana
 * @on 1/19/2022
 */
public interface GlobalService {
    public FileResponse storeFile(MultipartFile file, UUID by, String entity) throws IOException;
    public void deleteFile(UUID by, String entity);
    public void deleteFiles(List<UUID> bys, String entity);
}
