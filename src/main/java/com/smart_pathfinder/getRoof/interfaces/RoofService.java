package com.smart_pathfinder.getRoof.interfaces;

import com.smart_pathfinder.getRoof.exception.ResourceNotFoundException;
import com.smart_pathfinder.getRoof.models.Roof;
import com.smart_pathfinder.getRoof.payload.request.RoofRequest;
import com.smart_pathfinder.getRoof.payload.response.RoofResponse;

import java.util.UUID;

/**
 * @author William K
 * @on 1/20/2022
 */
public interface RoofService {
    // status between (true = private) and (false = public)

    public RoofResponse getRoofStandard(int page, int size);
    public Roof createRoof(RoofRequest roofRequest);
    public Roof updateRoof(Roof roof);
    public void deleteRoof(String id);
    public Roof getRoofById(boolean status, String id);
    public RoofResponse getRoofsByUserId(boolean status, String userId, int page, int size) throws ResourceNotFoundException;
    public Roof bookOrCancelRoof(int operation, String id);
    // public RoofResponse filterRoof(int page, int size);
}
