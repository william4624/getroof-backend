package com.smart_pathfinder.getRoof.advice;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage {
    int value;
    Date date;
    String message;
    String description;
}
