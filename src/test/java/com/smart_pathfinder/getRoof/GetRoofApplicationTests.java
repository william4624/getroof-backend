package com.smart_pathfinder.getRoof;

import com.smart_pathfinder.getRoof.models.*;
import com.smart_pathfinder.getRoof.payload.request.SignupRequest;
import com.smart_pathfinder.getRoof.services.RoleService;
import com.smart_pathfinder.getRoof.services.RoofService;
import com.smart_pathfinder.getRoof.services.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetRoofApplicationTests {
	@Autowired
	private RoleService roleService;

	@Autowired
	private UserService userService;

	@Autowired
	private RoofService roofService;

	/*@Test
	void contextLoads() {
		System.out.println("STARTING TEST");
	}*/

	// @Test
	public void createRoles() {
		if (roleService.findAll().size() == 0) {
			Role roleUser = new Role();
			roleUser.setName(ERole.ROLE_USER);

			Role roleEdit = new Role();
			roleEdit.setName(ERole.ROLE_EDIT);

			Role roleCreate = new Role();
			roleCreate.setName(ERole.ROLE_CREATE);

			Role roleDelete = new Role();
			roleDelete.setName(ERole.ROLE_DELETE);

			Role roleAdmin = new Role();
			roleAdmin.setName(ERole.ROLE_ADMIN);


			roleService.saveAll(List.of(roleUser, roleEdit, roleCreate, roleDelete, roleAdmin));
			List<Role> roles = roleService.findAll();
			assertThat(roles.size()).isEqualTo(5);
		}
	}

	@Test
	public void registerUsers() {
		createRoles();

		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		List<SignupRequest> signupRequests = new ArrayList<>();
		for (int i = 1; i < 3; ++i) {
			SignupRequest signupRequest = new SignupRequest();
			signupRequest.setUsername("test_user" + i);
			signupRequest.setEmail("test_user"+ i + "@gmail.com");
			signupRequest.setFirstName("firstname " + i);
			signupRequest.setLastName("lastname " + i);
			signupRequest.setAge(35);
			signupRequest.setDateOfBirth(LocalDate.parse("2020-12-16 10:30:00", df));
			signupRequest.setPassword("password");

			Set<String> roles = new HashSet<>();
			if (i % 2 == 0) {
				roles.add("user");
			} else {
				roles.add("admin");
			}
			signupRequest.setRoles(roles);

			AddressUser addressUser = new AddressUser();
			addressUser.setStreetAddress("Muster Str " + i);
			addressUser.setHouseNumber( String.valueOf(i) );
			addressUser.setZipCode("12740".replace("0", String.valueOf(i)) );
			addressUser.setCity("Duisburg");
			addressUser.setState("NRW");
			signupRequest.setAddress(addressUser);

			Set<String> languages = new HashSet<>();
			Set<String> phoneNumbers = new HashSet<>();
			for (int j = 0; j < 2; ++j) {
				if (j == 0) {
					languages.add("EN");
					phoneNumbers.add("1183722323");
					signupRequest.setDescription("that's my description");
				} else {
					languages.add("FR");
					phoneNumbers.add("33083722323");
					signupRequest.setDescription("voici ma description");
				}
			}
			signupRequest.setLanguages(languages);
			signupRequest.setPhoneNumbers(phoneNumbers);

			signupRequests.add(signupRequest);

			// signupRequest savedsignupRequest = user.save(user);
			// createRoof(savedUsr);

		}
		for (SignupRequest signupRequest: signupRequests) {
			System.out.println(signupRequest.toString());

			userService.register(signupRequest);
		}
	}

	// @AfterTestClass
	/*public void createRoof() {
		List<User> users = userService.getUsers();

		for (User user: users) {

			List<RoofRequest> roofRequests = new ArrayList<>();
			for (int i = 0; i < 100; ++i) {
				RoofRequest roof = new RoofRequest();
				roof.setUser(user);
				roof.setCurrency(ECurrency.FCFA);
				roof.setNumberLivingRoom(2);
				roof.setNumberBedroom(3);

				Set<LanguageRoof> languageRoofs = new HashSet<>();
				for (int k = 0; k < 2; ++k) {
					LanguageRoof languageRoof = new LanguageRoof();
					if (k == 0) {
						languageRoof.setName(ELanguage.EN);
					} else {
						languageRoof.setName(ELanguage.FR);
					}

					languageRoofs.add(languageRoof);
				}
				roof.setAvailableLanguages(languageRoofs);

				Set<RoofType> eRoofs = new HashSet<>();
				eRoofs.add(new RoofType(ERoofType.PRIVATE));
				roof.setTypes(eRoofs);

				AddressRoof addressRoof = new AddressRoof();
				addressRoof.setStreetAddress("Muster Str");
				addressRoof.setHouseNumber("12");
				addressRoof.setZipCode("12740");
				addressRoof.setCity("Duisburg");
				addressRoof.setState("NRW");

				roof.setAddress(addressRoof);


				if (i % 2 == 0) {
					roof.setCurrency(ECurrency.FCFA);
					roof.setPrice(1000 * 2);
					roof.setDescription("french description " + i);
					roof.setTitle("Le texte qui suit est la titre " + i);
				} else {
					roof.setCurrency(ECurrency.EURO);
					roof.setPrice(5 * 2);
					roof.setDescription("europe description " + i);
					roof.setTitle("Text that follows is for title " + i);
				}

				int status = i % 3;
				switch (status) {
					case 0:
						roof.setStatus((byte) 2);
						break;
					case 1:
						roof.setStatus((byte) 3);
						break;
					default:
						roof.setStatus((byte) 1);
				}

				// PROMO on Roof
				if (i % 5 == 0) {
					roof.setPromoPercentage(10);
					roof.applyPromoCode();
				}
				roof.setCreatedAt(LocalDateTime.now());

				roofs.add(roof);
			}
		}

		roofRepository.saveAll(roofs);

	}*/

}
