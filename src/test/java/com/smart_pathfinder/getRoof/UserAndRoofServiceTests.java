package com.smart_pathfinder.getRoof;

import com.smart_pathfinder.getRoof.exception.ResourceNotFoundException;
import com.smart_pathfinder.getRoof.models.*;
import com.smart_pathfinder.getRoof.repository.RoleRepository;
import com.smart_pathfinder.getRoof.repository.RoofRepository;
import com.smart_pathfinder.getRoof.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class UserAndRoofServiceTests {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    RoofRepository roofRepository;

    public String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    public void createRoles() {
        if (roleRepository.findAll().size() == 0) {
            Role roleUser = new Role();
            roleUser.setName(ERole.ROLE_USER);

            Role roleEdit = new Role();
            roleEdit.setName(ERole.ROLE_EDIT);

            Role roleCreate = new Role();
            roleCreate.setName(ERole.ROLE_CREATE);

            Role roleDelete = new Role();
            roleDelete.setName(ERole.ROLE_DELETE);

            Role roleAdmin = new Role();
            roleAdmin.setName(ERole.ROLE_ADMIN);


            roleRepository.saveAll(List.of(roleUser, roleEdit, roleCreate, roleDelete, roleAdmin));
            List<Role> roles = roleRepository.findAll();
            assertThat(roles.size()).isEqualTo(5);
        }
    }

    @Test
    public void registerUser() {
        createRoles();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        List<User> users = new ArrayList<>();
        for (int j = 0; j < 2; ++j) {
            User user = new User();
            user.setUsername("test_user" + j);
            user.setEmail("test_user"+ j + "@gmail.com");
            user.setFirstName("firstname " + j);
            user.setLastName("lastname " + j);
            user.setAge(35);
            user.setDateOfBirth(LocalDate.parse("2020-12-16 10:30:00", df));

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodedPassword = encoder.encode("password" + j);
            user.setPassword(encodedPassword);

            Set<Role> roles = new HashSet<>();

            if (j % 2 == 0) {
                Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new ResourceNotFoundException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                        .orElseThrow(() -> new ResourceNotFoundException("Error: Role is not found."));
                roles.add(userRole);
            }
            user.setRoles(roles);

            List<AddressUser> addresses = new ArrayList<>();
            for (int i = 0; i < 3; ++i) {
                AddressUser addressUser = new AddressUser();
                addressUser.setStreetAddress("Muster Str " + i);
                addressUser.setHouseNumber( String.valueOf(i) );
                addressUser.setZipCode("12740".replace("0", String.valueOf(i)) );
                addressUser.setCity("Duisburg");
                addressUser.setState("NRW");

                addresses.add(addressUser);
            }
            user.setAddress(addresses);

            Set<LanguageUser> languageUsers = new HashSet<>();
            for (int i = 0; i < 2; ++i) {
                LanguageUser language = new LanguageUser();
                if (i == 0) {
                    language.setName(ELanguage.EN);
                    user.setDescription("that's my description");
                } else {
                    language.setName(ELanguage.FR);
                    user.setDescription("voici ma description");
                }

                languageUsers.add(language);
            }
            user.setLanguages(languageUsers);
            user.setCreatedAt(LocalDateTime.now());
            users.add(user);

            User savedUsr = userRepository.save(user);
            createRoof(savedUsr);

        }
        userRepository.saveAll(users);
    }

    public void createRoof(User user) {
        List<Roof> roofs = new ArrayList<>();

        for (int i = 0; i < 100; ++i) {
            Roof roof = new Roof();
            roof.setUser(user);
            roof.setCurrency(ECurrency.FCFA);
            roof.setNumberLivingRoom(2);
            roof.setNumberBedroom(3);

            Set<LanguageRoof> languageRoofs = new HashSet<>();
            for (int k = 0; k < 2; ++k) {
                LanguageRoof languageRoof = new LanguageRoof();
                if (k == 0) {
                    languageRoof.setName(ELanguage.EN);
                } else {
                    languageRoof.setName(ELanguage.FR);
                }

                languageRoofs.add(languageRoof);
            }
            roof.setAvailableLanguages(languageRoofs);

            Set<RoofType> eRoofs = new HashSet<>();
            eRoofs.add(new RoofType(ERoofType.PRIVATE));
            roof.setTypes(eRoofs);

            AddressRoof addressRoof = new AddressRoof();
            addressRoof.setStreetAddress("Muster Str");
            addressRoof.setHouseNumber("12");
            addressRoof.setZipCode("12740");
            addressRoof.setCity("Duisburg");
            addressRoof.setState("NRW");

            roof.setAddress(addressRoof);


            if (i % 2 == 0) {
                roof.setCurrency(ECurrency.FCFA);
                roof.setPrice(1000 * 2);
                roof.setDescription("french description " + i);
                roof.setTitle("Le texte qui suit est la titre " + i);
            } else {
                roof.setCurrency(ECurrency.EURO);
                roof.setPrice(5 * 2);
                roof.setDescription("europe description " + i);
                roof.setTitle("Text that follows is for title " + i);
            }

            int status = i % 3;
            switch(status) {
                case 0:
                    roof.setStatus((byte) 2);
                    break;
                case 1:
                    roof.setStatus((byte) 3);
                    break;
                default:
                    roof.setStatus((byte) 1);
            }

            // PROMO on Roof
            if (i % 5 == 0) {
                roof.setPromoPercentage(10);
                roof.applyPromoCode();
            }
            roof.setCreatedAt(LocalDateTime.now());

            roofs.add(roof);
        }

        roofRepository.saveAll(roofs);

    }

    // a method to clean up after testing
}
